from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest
from .models import Friends
from .views import index, add_friend, delete, url_is_valid

# Create your tests here.
class AddFriendTest(TestCase):
	def test_add_friend_is_exist(self):
		response = Client().get('/add-friend/')
		self.assertEqual(response.status_code,200)

	def test_model_can_add_new_friend(self):
		new_friend = Friends.objects.create(friend = 'Jonas Hector', url = 'http://test.com')
		counting_all_available_friends = Friends.objects.all().count()
		self.assertEqual(counting_all_available_friends, 1)

	def test_add_friend_using_index_funct(self):
		found = resolve('/add-friend/')
		self.assertEqual(found.func, index)
	
	def test_add_friend_post_success_and_render_the_result(self):
		test_name = 'kono'
		test_url = 'http://test.com'
		response_post = Client().post('/add-friend/add_friend/', {'name':test_name, 'url':test_url})
		self.assertEqual(response_post.status_code, 302)
	
		response = Client().get('/add-friend/')
		html_response = response.content.decode('utf8')
		self.assertIn(test_name, html_response)
		self.assertIn(test_url, html_response)
		
	def test_add_friend_post_error_and_render_the_result(self):
		test_name = 'Jonas Hector'
		test_url = 'http://test.com'
		response_post = Client().post('/add-friend/add_friend/', {'name':'', 'url':''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/add-friend/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test_name, html_response)
		self.assertNotIn(test_url, html_response)
	
	def test_delete(self):
		new_activity = Friends.objects.create(
		friend='Jonas Hector', url='http://test.com')
		delete(self, new_activity.id)
		counting_all_available_todo = Friends.objects.all().count()
		self.assertEqual(counting_all_available_todo, 0)
		
	def test_url_validator(self):
		not_valid_url = 'http://jkhrginkjer.com'
		valid_url = 'http://google.com'
		self.assertEqual(url_is_valid(not_valid_url), False)
		self.assertEqual(url_is_valid(valid_url), True)
