from django import forms

class Friend_form(forms.Form):
	error_messages = {
		'required': 'Tolong isi input ini',
		'invalid': 'Isi input dengan link',
	}
	name_attrs = {
		'type' : 'text',
		'class' : 'form-control',
		'placeholder' : 'Masukkan nama teman Anda'
	}
	url_attrs = {
		'type' : 'url',
		'class' : 'form-control',
		'placeholder' : 'Masukkan URL teman Anda'
	}

	name = forms.CharField(label='Nama', required=True, widget=forms.TextInput(attrs=name_attrs))
	url = forms.URLField(label = 'URL', required = True, widget=forms.TextInput(attrs=url_attrs))