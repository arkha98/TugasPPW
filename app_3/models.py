from django.db import models

# Create your models here.
class Friends(models.Model):
    friend = models.TextField(max_length = 30)
    url = models.URLField()
    date = models.DateTimeField(auto_now_add = True)
