from django.conf.urls import url
from .views import index
from .views import add_friend, delete
#url for app
urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'add_friend/$', add_friend, name='add_friend'),
	url(r'^delete/(?P<id>\w{0,50})/$', delete, name='delete')
]
