from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Friends
from .forms import Friend_form
from urllib.request import urlopen
from urllib.error import URLError

# Create your views here.
friends_dict={}
def index(request):
	friend = Friends.objects.all()
	friends_dict['friend'] = friend
	html = 'app_3/add_friend.html'
	friends_dict['add_form'] = Friend_form
	return render(request, html, friends_dict)

def add_friend(request):
	form = Friend_form(request.POST or None)
	friends_dict['valid'] = True
	if (request.method == 'POST' and form.is_valid() and url_is_valid(request.POST['url'])):
		friends_dict['name'] = request.POST['name']
		friends_dict['url'] = request.POST['url']
		friend = Friends(friend=friends_dict['name'], url=friends_dict['url'])
		friend.save()
		return HttpResponseRedirect('/add-friend/')
	else:
		friends_dict['valid'] = False
		return HttpResponseRedirect('/add-friend/')

def url_is_valid(url):
    try:
        thepage = urlopen(url)
    except URLError as e:
        return False
    else:
        return True

def delete(request, id):
    instance = Friends.objects.get(id=id)
    instance.delete()
    return HttpResponseRedirect('/add-friend/')
