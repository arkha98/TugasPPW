App-4
[![build status](https://gitlab.com/arkha98/TugasPPW/badges/master/build.svg)](https://gitlab.com/arkha98/TugasPPW/commits/master) 
[![coverage report](https://gitlab.com/arkha98/TugasPPW/badges/master/coverage.svg)](https://gitlab.com/arkha98/TugasPPW/commits/master)

# Lab 5: Tugas _PPW_

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## Anggota Kelompok

- Arkha Sayoga Mayadi
- Fari Qodri Andana
- Hema Mitta Kalyani
- Rico Putra Pradana

## Heroku App Link

https://ppw-lingin.herokuapp.com