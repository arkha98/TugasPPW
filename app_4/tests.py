# -*- coding: utf-8 -*-
from django.test import TestCase
from django.test import Client
from django.test import TestCase
from django.urls import resolve
from .views import index
# from __future__ import unicode_literals

# Create your tests here.
class TestApp4(object):
	"""docstring for TestApp4"""
	def test_app_4_url_is_exist(self):
		response = Client().get('/stats/')
		self.assertEqual(response.status_code, 200)

	def test_app_4_using_index_func(self):
		found = resolve('/stats/')
		self.assertEqual(found.func, index)
