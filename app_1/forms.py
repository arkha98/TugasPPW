from django import forms

class Status_Form(forms.Form):
	status_attrs = {
		'class': 'form-control',
	}

	description = forms.CharField(label='', required=True, max_length=280, widget=forms.TextInput(attrs=status_attrs))