from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

# Create your views here.
response = {}

def index(request):
	response['author'] = "Rico Putra Pradana"
	html = 'app_1/app_1.html'
	response['status'] = Status.objects.all()[::-1]
	response['status_form'] = Status_Form
	return render(request, html, response)

def add_status(request):
	form = Status_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['description'] = request.POST['description']
		status = Status.objects.create(description=response['description'])
		status.save()
		html = 'app_1/app_1.html'
		render(request,html,response)
		return HttpResponseRedirect('/update-status/')
	else:
		return HttpResponseRedirect('/update-status/')