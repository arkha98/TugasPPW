from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status
from .models import Status
from .forms import Status_Form

# Create your tests here.
class App1UnitTest(TestCase):

	def test_app_1_url_is_exist(self):
		response = Client().get('/update-status/')
		self.assertEqual(response.status_code, 200)

	def test_app1_using_index_func(self):
		found = resolve('/update-status/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_status(self):
		# Creating a new activity
		new_activity = Status.objects.create(description='Test membuat status')

		# Retrieving all availability activity
		counting_all_availability_status = Status.objects.all().count()
		self.assertEqual(counting_all_availability_status, 1)

	def test_form_status_input_has_css_clases(self):
		form = Status_Form()
		self.assertIn('class="form-control"', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = Status_Form(data={'status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['description'],
			["This field is required."]
		)

	def test_app1_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/update-status/add_status', {'description': test})
		self.assertEqual(response_post.status_code, 302)

		reponse = Client().get('/update-status/')
		html_response = reponse.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_app1_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/update-status/add_status', {'description': ''})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/update-status/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)