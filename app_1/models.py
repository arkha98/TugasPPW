from django.db import models

# Create your models here.
class Status(models.Model):
	description = models.CharField(max_length=280)
	created_date = models.DateTimeField(auto_now_add=True)