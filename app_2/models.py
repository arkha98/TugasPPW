from django.db import models
from datetime import date

class Profile(models.Model):
	name = models.CharField(max_length=30)
	gender = models.CharField(max_length=6)
	description = models.CharField(max_length=100)
	email = models.EmailField()

class Expertise(models.Model):
	expertise = models.CharField(max_length=255)