from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Profile, Expertise

class App2UnitTest(TestCase):

	def test_app_2_url_is_exist(self):
		response = Client().get('/profile/')
		self.assertEqual(response.status_code,200)

	def test_app_2_using_index_func(self):
		found = resolve('/profile/')
		self.assertEqual(found.func, index)