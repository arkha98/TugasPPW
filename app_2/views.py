from django.shortcuts import render
from datetime import date
from .models import Profile, Expertise

# Create your views here.

mhs_name = 'Lee Jong Suk'
birth_date = date(1989, 9, 14)
mhs_gender = 'Male'
mhs_expertise = ['Actor', 'Model', 'Singer']
mhs_description = 'Currently starring in While You Were Sleeping'
mhs_email = 'jongsuk0206@daum.net'


def index(request):
    profile = Profile(name=mhs_name,gender=mhs_gender, description=mhs_description, email=mhs_email)
    expertise = Expertise(expertise=mhs_expertise)
    response = {'Name': profile.name,'Birthday': birth_date,'Gender':profile.gender, 'Description':profile.description, 'Email': profile.email, 'Expert' : expertise.expertise}
    return render(request, 'app_2/app_2.html', response)

